# Ludum Dare : Minimalism

This is my Ludum Dare entry for the 48-hour solo compo in the weekend of April
26-29. The theme was *Minimalism*.

The tag `LD48` shows the code at the state that was submitted to the Ludum Dare
compo.

Newest version uses the [Crafty-bmViewport][cb] library for better viewport
following.

I have also written [postmortem][pm] going into detail about my experience
writing this game under the immense time pressure of Ludum Dare.

All the code here is available under the [Apache License, Version 2.0][al]. The
assets are licensed under the [Creative Commons Attribution-ShareAlike 3.0 Unported License][cc].

[al]: http://www.apache.org/licenses/LICENSE-2.0
[cc]: http://creativecommons.org/licenses/by-sa/3.0/
[cb]: https://github.com/berdon/Crafty-bmViewport
[pm]: http://rabbe.tumblr.com/post/49474760531/ludum-dare-hell-hole

