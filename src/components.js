//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// components.js
//
// Declares different components used in the game.
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Basics
//------------------------------------------------------------------------------

//
// A tile is a grid is a tile is a grid is a wha---?
//

Crafty.c(
    // name
    'Grid',

    // component
    {
        init: function () {
            this.attr({
                w: Game.map_grid.tile.width,
                h: Game.map_grid.tile.height
            })
        },

        at: function (x, y) {
            if (x === undefined && y === undefined) {
                return {
                    x: this.x / Game.map_grid.tile.width,
                    y: this.y / Game.map_grid.tile.height
                };
            } else {
                this.attr({
                    x: x * Game.map_grid.tile.width,
                    y: y * Game.map_grid.tile.height
                });

                return this;
            }
        }
    }
);

//
// It's the basics, y'know?
//

Crafty.c(
    // name
    'Actor',

    // component
    {
        init: function () {
            this.requires('2D, Canvas, Grid, Collision');
        }
    }
);

//
// "Thou know'st 'tis common; all that lives must die,
//  passing through nature to eternity."
//

Crafty.c(
    // name
    'Dies',

    // component
    {
        init: function () {
            this.attr({ dead: false });
        },

        die: function () {
            this.attr({ dead: true })
                .trigger('Died', this);
        }
    }
);

//
// “Killing is not so easy as the innocent believe.”
//

Crafty.c(
    // name
    'Kills',

    // component
    {
        init: function () {
            this.requires('Collision')
                .attr({ _target: undefined });
        },

        targets: function (target) {
            this.attr({ _target: target })
                .onHit(this._target, this.hitFunction);

            return this;
        },

        cannotKill: function () {
            this.attr({ _target: undefined });

            return this;
        },

        hitFunction: function (data) {
            var t = data[0].obj;
            if (t.has(this._target) && t.has('Dies')) {
                t.die();
                this.trigger('Killed', t);
            }
        }
    }
);

//
// "I'm sorry, Dave. I'm afraid I can't do that."
//

Crafty.c(
    // name
    'AI',

    // component
    {
        init: function () {
            this.requires('2D Grid')
                .attr({ player: undefined, speed: 4 })
                .bind('EnterFrame', this.doAIStuff);
        },

        doAIStuff: function () {
            if (this.player === undefined) {
                // random walk

                return;
            }

            var vThis = new Crafty.math.Vector2D(this._x, this._y);
            var vPlayer = new Crafty.math.Vector2D(this.player._x, this.player._y);

            if (vThis.distance(vPlayer) < 250) {
                // then start chasing if we can see him.
                var angle = vThis.angleTo(vPlayer); // NOTE radians

                // walk tiles between here and player, are there walls?


                var quad = this._quadrantForAngle(Crafty.math.radToDeg(angle));
                this.move(quad, this.speed);
            }
        },

        setPlayer: function (player) {
            this.player = player;
        },

        _quadrantForAngle: function (angle) {
            if (Crafty.math.withinRange(angle, -45, 45))
                return 'e';
            if (Crafty.math.withinRange(angle, -135, -45))
                return 'n';
            if (Crafty.math.withinRange(angle, 45, 135))
                return 's';
            if (Crafty.math.withinRange(angle, -180, -135) ||
                Crafty.math.withinRange(angle, 135, 180))
                return 'w';
        }
    }
);

//
// It's like a bag of holding
//

Crafty.c(
    // name
    'Inventory',

    {
        init: function () {
            this.ectoplasms = 0;
            this.hasThing = false;
            this.blipSize = 8

            this.bind('Draw', function (e) {
                // draw items
                if (e.type === "DOM") {
                    // TODO fix for DOM if we ever need it.
                } else if (e.type === "canvas") {
                    var offset = 0;
                    if (this.hasThing) {
                        offset = this.blipSize * 1.5;

                        e.ctx.fillStyle = "#FF0000";
                        e.ctx.fillRect(
                            e.pos._x,
                            e.pos._y,
                            this.blipSize,
                            this.blipSize);
                    }

                    e.ctx.fillStyle = "#00FF00";
                    for (var i = 0; i < this.ectoplasms; i++)
                        e.ctx.fillRect(
                            e.pos._x + this.blipSize * 1.5 * i + offset,
                            e.pos._y,
                            this.blipSize,
                            this.blipSize);
                }
            });
        },

        pickupEctoplasm: function (data) {
            this.ectoplasms++;
            ectoplasm = data[0].obj;
            ectoplasm.collect();
        },

        useEctoplasm: function () {
            if (this.ectoplasms > 0) {
                this.ectoplasms--;
                this.trigger('Change');
            }
        },

        pickupThing: function (data) {
            this.hasThing = true;
            thing = data[0].obj;
            thing.collect();
        }
    }
);

//
// "Keep firing assholes!"
//

Crafty.c(
    // name
    'Bullet',

    // component
    {
        init: function () {
            this.requires('Collision, Kills')
                .attr({ speed: 5, offset: undefined })
                .bind('EnterFrame', function () {
                    // setup offset for this bullet.
                    if (this.offset === undefined) {
                        this.offset = { x: 0, y: 0 };
                        switch(this._rotation) {
                        case 0:
                            this.offset.x =  this.speed;
                            break;
                        case 90:
                            this.offset.y = this.speed;
                            break;
                        case 180:
                            this.offset.x = -this.speed;
                            break;
                        case 270:
                            this.offset.y = -this.speed;
                            break;
                        }
                    }

                    this.x += this.offset.x;
                    this.y += this.offset.y;
                })
                .targets('Demon')
                .bind('Killed', function () {
                    this.destroy();
                })
                .onHit('Wall', function () {
                    this.destroy();
                });
                // Can we check if it leaves the game and destroy it?
        }
    }
);

//------------------------------------------------------------------------------
// Terrain
//------------------------------------------------------------------------------

//
// "Thou shalt not pass!!!"
//

Crafty.c(
    // name
    'Water',

    // component
    {
        init: function () {
            this.requires('Actor, Color')
                .color('blue');
        }
    }
);

//
// "Wait?!? I can walk through this ****?"
//

Crafty.c(
    // name
    'Wall',

    // component
    {
        init: function () {
            this.requires('Actor, Color')
                .color('black');
        }
    }
);

//
// "It was like a view you would never imagine. Death, destruction, and fire."
//

Crafty.c(
    // name
    'HellHole',

    // component
    {
        init: function() {
            this.requires('Actor, Color')
                .color('red');
        }
    }
);

//------------------------------------------------------------------------------
// Player
//------------------------------------------------------------------------------

//
// Our protagonist. A ghost searching for something, but what???
//

Crafty.c(
    // name
    'Ghostie',

    // component
    {
        init: function () {
            this.direction = 90;

            this.requires(
                    'Actor, Dies, Inventory, Fourway, Keyboard, \
                    SpriteAnimation, spr_ghostie_down, ViewportFollow')
                .fourway(4)
                .collision(
                    [12, 63], [12, 24], [23, 12], [38, 12], [52, 24], [52, 63])
                .viewportFollow(218, new Crafty.polygon(
                    [0,0], [Game.width(), 0],
                    [Game.width(), Game.height()], [0, Game.height()]))
                .onHit('Water', this.stopMovement)
                .onHit('Ectoplasm', this.pickupEctoplasm)
                .onHit('Thing', this.pickupThing)
                .onHit('HellHole', this.die)
                .bind('NewDirection', function(data) {
                    // Watch for a change of direction and switch sprite.
                    if (data.x > 0) {
                        this.direction = 0;
                        this.addComponent('spr_ghostie_right');
                    } else if (data.x < 0) {
                        this.direction = 180;
                        this.addComponent('spr_ghostie_left');
                    } else if (data.y > 0) {
                        this.direction = 90;
                        this.addComponent('spr_ghostie_down');
                    } else if (data.y < 0) {
                        this.direction = 270;
                        this.addComponent('spr_ghostie_up');
                    }
                })
                .bind('KeyDown', function (e) {
                    if (e.key == Crafty.keys['SPACE']) {
                        // fire!
                        this.useEctoplasm();
                        Crafty.e('EctoShot')
                            .attr(this.getShotPositionAndDirection());
                    }
                })
                .bind('Died', function () {
                    this.bind('AnimationEnd', this.trueDeath)
                        .animate('Absolution', 10, 0);
                })
                .animate('Absolution', [[3, 2], [4, 2]]);
        },

        stopMovement: function () {
            this._speed = 0;
            if (this._movement)
            {
                this.x -= this._movement.x;
                this.y -= this._movement.y;
            }
        },

        trueDeath: function () {
            this.unbind('AnimationEnd');
            this.destroy();
            Crafty.trigger('DeathOfGhostie');
        },

        // utility

        getShotPositionAndDirection: function () {
            var x = this._x;
            var y = this._y;

            // this makes no sense at all!!!
            // NOTE 0 = right, 90 = down, 180 = left, 270 = up
            switch (this.direction) {
            case 0:
                x += Game.map_grid.tile.width;
                break;
            case 90:
                x += Game.map_grid.tile.width;
                y += Game.map_grid.tile.height;
                break;
            case 180:
                y += Game.map_grid.tile.height;
                break;
            case 270:
                break;
            };

            return { rotation: this.direction, x: x, y: y };
        }
    }
);

//------------------------------------------------------------------------------
// Items/Pickups/Interactive entities.
//------------------------------------------------------------------------------

//
// W00t, ectoplasm!
//

Crafty.c(
    // name
    'Ectoplasm',

    // component
    {
        init: function() {
            this.requires('Actor, spr_ecto')
                .collision(
                    [0, 47], [5, 42], [24, 36], [39, 36], [58, 42], [63, 47],
                    [63, 52], [58, 57], [39, 63], [24, 63], [5, 57], [0, 52]);
        },

        collect: function () {
            this.destroy();
            // NOTE cannot use 'this' in event functions, it's destroyed.
            Crafty.trigger('EctoplasmPickedUp', this);
        }
    }
);

//
// Pew pew pew
//

Crafty.c(
    // name
    'EctoShot',

    // component
    {
        init: function () {
            this.requires('Actor, Bullet, spr_ecto_shot')
                .collision(
                    [4, 29], [32, 18], [38, 18], [46, 21], [49, 27], [49, 32],
                    [46, 38], [38, 41], [32, 41], [4, 30])
        }
    }
);

//
// What _is_ this thing!?!?
//

Crafty.c(
    // name
    'Thing',

    // component
    {
        init: function () {
            this.requires('Actor, spr_thing')
                .collision(
                    [12, 63], [12, 24], [23, 12], [38, 12], [52, 24], [52, 63]);
        },

        collect: function () {
            this.destroy();
            // NOTE cannot use 'this' in event functions, it's destroyed.
            Crafty.trigger('ThingPickedUp', this);
        }
    }
);

//------------------------------------------------------------------------------
// NPCs (friendlies, enemies, etc.)
//------------------------------------------------------------------------------

//
// He's an ugly one alright.
//

Crafty.c(
    // name
    'Demon',

    // component
    {
        init: function () {
            this.requires(
                'Actor, AI, Dies, Kills, SpriteAnimation, spr_demon_down')
                .collision(
                    [12, 63], [12, 24], [23, 12], [38, 12], [52, 24], [52, 63])
                .targets('Ghostie');

            // Watch for a change of direction and switch sprite accordingly
            this.bind('NewDirection', function(data) {
                    if (data.x > 0) {
                        this.addComponent('spr_demon_right');
                    } else if (data.x < 0) {
                        this.addComponent('spr_demon_left');
                    } else if (data.y > 0) {
                        this.addComponent('spr_demon_down');
                    } else if (data.y < 0) {
                        this.addComponent('spr_demon_up');
                    }
                })
                .bind('Died', function () {
                    this.cannotKill()
                        .bind('AnimationEnd', this.trueDeath)
                        .animate('DemonDeath', 10, 0);
                })
                .animate('DemonDeath', [[4, 0], [0, 1]]);
        },

        trueDeath: function () {
            this.unbind('AnimationEnd');
            this.destroy();
            Crafty.trigger('DemonicDeath');
        }
    }
);


