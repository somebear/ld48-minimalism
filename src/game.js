//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// game.js
//
// The main game file for the LD48 Minimalism game
//------------------------------------------------------------------------------

Game = {
    // map size
    map_grid: {
        width: 32,
        height: 18,
        tile: {
            width: 64,
            height: 64
        }
    },

    VIEWPORT_WIDTH: 500,
    VIEWPORT_HEIGHT: 500,

    width: function () {
        return this.map_grid.width * this.map_grid.tile.width;
    },

    height: function () {
        return this.map_grid.height * this.map_grid.tile.height;
    },

    // Initialize and start our game
    start: function() {
        // Start crafty and set a background color so that we can see it's working
        Crafty.init(Game.width(), Game.height());
        Crafty.background('white');
        Crafty.viewport.init(Game.VIEWPORT_WIDTH, Game.VIEWPORT_HEIGHT);

        Crafty.scene('Loading');
    }
}

$text_css = {
    'font-size': '24px',
    'font-family': '"Helvetica Neue", Arial',
    'font-weight': 'bold',
    'font-style': 'italic',
    'color': '#333',
    'text-align': 'center' };

