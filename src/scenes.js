//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// scene.js
//
// The scenes used in the game.
//------------------------------------------------------------------------------

//
// ???. LOADING    ???
//

Crafty.scene(
    // name
    'Loading',

    // init
    function () {
        this.loadingText = Crafty.e('2D, DOM, Text')
            .text('Loading...')
            .attr({
                x: 0,
                y: Game.VIEWPORT_HEIGHT / 2 - 24,
                w: Game.VIEWPORT_WIDTH })
            .css($text_css);

        // Load our sprite map image
        Crafty.load(['assets/Ghostie@4x.png', 'assets/Hell_Hole@4x.png'], function () {
            // Once the image is loaded...

            // Define the individual sprites in the image
            // Each one (spr_tree, etc.) becomes a component
            // These components' names are prefixed with "spr_"
            //    to remind us that they simply cause the entity
            //    to be drawn with a certain sprite
            Crafty.sprite(64, 'assets/Ghostie@4x.png', {
                spr_ghostie_pre_death: [0, 0]
            });

            Crafty.sprite(64, 'assets/Hell_Hole@4x.png', {
                spr_ghostie_down: [4, 1],
                spr_ghostie_up: [0, 2],
                spr_ghostie_right: [1, 2],
                spr_ghostie_left: [2, 2],
                spr_demon_down: [0, 0],
                spr_demon_up: [1, 0],
                spr_demon_right: [2, 0],
                spr_demon_left: [3, 0],
                spr_ecto: [2, 1],
                spr_ecto_shot: [3, 1],
                spr_thing: [1,1]
            });

            // Now that our sprites are ready to draw, start the game
            Crafty.scene('PassingThrough');
        });
    }
);

//
// EXT. SEEDY ALLEY    DAY?
//
// Introduction, a short animated series in which our hero dies (killed) and
// turns into a ghost fading into the spirit world.
//

Crafty.scene(
    // name
    'SeedyAlley',

    // init
    function () {

    },

    // uninit
    function () {

    }
);

//
// EXT.? SPIRIT WORLD    DAY?
//
// Where our hero moves around and learns the world.
//

Crafty.scene(
    // name
    'PassingThrough',

    // init
    function () {
        Crafty.viewport.x = 0;
        Crafty.viewport.y = 0;
        setTimeout(function() {
            Crafty.scene('SpiritWorld')
        }, 0);
    }
);

Crafty.scene(
    // name
    'SpiritWorld',

    // init
    function () {
        // A 2D array to keep track of all occupied tiles
        this.occupied = new Array(Game.map_grid.width);
        for (var x = 0; x < Game.map_grid.width; x++) {
            this.occupied[x] = new Array(Game.map_grid.height);
            for (var y = 0; y < Game.map_grid.height; y++) {
                this.occupied[x][y] = false;
            }
        }

        // Player character, placed at top left corner of our grid
        this.player = Crafty.e('Ghostie').at(1, 1);
        this.occupied[this.player.at().x][this.player.at().y] = true;

        // Place a some random stuff
        for (var x = 0; x < Game.map_grid.width; x++) {
            for (var y = 0; y < Game.map_grid.height; y++) {
                var at_edge = x == 0 || x == Game.map_grid.width - 1 || y == 0 || y == Game.map_grid.height - 1;

                if (at_edge) {
                    Crafty.e('Water').at(x, y);
                    this.occupied[x][y] = true;
                } else if (Math.random() < 0.06 && !this.occupied[x][y]) {
                    Crafty.e('Wall').at(x, y);
                    this.occupied[x][y] = true;
                } else if (Math.random() < 0.01 && !this.occupied[x][y]) {
                    Crafty.e('Water').at(x, y);
                    this.occupied[x][y] = true;
                }
            }
        }

        // Place twice as many ectos as demons
        var max_ectos = 10;
        var max_demons = 5;

        var ectos = 0;
        while (ectos < max_ectos) {
            var x = Math.floor(Math.random() * (Game.map_grid.width - 1));
            var y = Math.floor(Math.random() * (Game.map_grid.height - 1));

            if (!this.occupied[x][y]) {
                Crafty.e('Ectoplasm').at(x, y);
                this.occupied[x][y] = true;
                ectos++;
            }
        }

        var demons = 0
        while (demons < max_demons) {
            var x = Math.floor(Math.random() * (Game.map_grid.width - 1));
            var y = Math.floor(Math.random() * (Game.map_grid.height - 1));

            if (!this.occupied[x][y] &&
                (Crafty.math.distance(
                    this.player.at().x, this.player.at().y, x, y) > 16)) {
                Crafty.e('Demon').at(x, y).setPlayer(this.player);
                demons++;
                this.occupied[x][y] = true;
            }
        }

        // Start from scratch if you are killed.
        Crafty.bind('DeathOfGhostie', function () {
            Crafty.unbind('DeathOfGhostie');

            Crafty.e('2D, Canvas, Color, Tween')
                .attr({ x: 0, y: 0, w: Game.width(), h: Game.height(), alpha: 0.0 })
                .color('white')
                .tween({ alpha: 1.0 }, 50)
                .bind('TweenEnd', function () {
                    Crafty.unbind('TweenEnd');
                    Crafty.scene('PassingThrough');
                });
        });

        Crafty.bind('DemonicDeath', function () {
            if (Crafty('Demon').length == 0) {
                Crafty.unbind('DemonicDeath');

                Crafty.e('2D, Canvas, Color, Tween')
                    .attr({ x: 0, y: 0, w: Game.width(), h: Game.height(), alpha: 0.0 })
                    .color('white')
                    .tween({ alpha: 1.0 }, 50)
                    .bind('TweenEnd', function () {
                        Crafty.unbind('TweenEnd');
                        Crafty.scene('Absolution');
                    });
            }
        })
    },

    // uninit
    function () {
        // TODO have to delete both x & y?
        delete this.occupied;
    }
);


//
// EXT.? ABSOLUTION    DAY?
//
// Final animation where the spirit world fades away. Our hero smiles and his
// spirit disolves.
//

Crafty.scene(
    // name
    'Absolution',

    // init
    function () {
        Crafty.viewport.x = 0;
        Crafty.viewport.y = 0;

        this.absolution = Crafty.e('2D, DOM, Text')
            .text('Absolution!')
            .attr({
                x: 0,
                y: Game.VIEWPORT_HEIGHT / 2 - 24,
                w: Game.VIEWPORT_WIDTH })
            .css($text_css);

        this.restart_game = this.bind('KeyDown', function() {
            Crafty.scene('PassingThrough');
        });
    },

    // uninit
    function () {
        this.unbind('KeyDown', this.restart_game);
    }
);
